const encoder = new TextEncoder();
const fileContent = await Deno.readFile('./example-log.txt');
const decoder = new TextDecoder();
const logLines = decoder.decode(fileContent).split('\n');

export default function start(buffer: Deno.Buffer) {
    setInterval(() => {
        const randomLine = Math.floor(Math.min(Math.random() * 1000, logLines.length));
        buffer.write(encoder.encode(logLines[randomLine]));
    }, 100);
}
