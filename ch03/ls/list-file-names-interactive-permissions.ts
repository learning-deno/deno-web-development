const [path = "."] = Deno.args;

await Deno.permissions.request({
    name: "read",
    path: path,
});

for await (const dir of Deno.readDir(path)) {
    console.log(dir.name);
}
