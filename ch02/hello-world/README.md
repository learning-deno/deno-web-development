# Deno toolchain

## Hello world

First deno application. To runit execute following command:

```shell
deno run my-first-deno-program.js
```

It will produce following output:

```shell
Hello from deno
```

## Modules and third-party dependencies

When run following command for the first time:

```shell
deno run hello-http-server.js
```

it will produce output like this:

```shell
Download https://deno.land/std@0.93.0/http/server.ts
Download https://deno.land/std@0.93.0/encoding/utf8.ts
...
error: Uncaught PermissionDenied: network access to "0.0.0.0:8080", run again with the --allow-net flag
```

This shows that deno is loading dependencies to the local cache. After
downloading and caching the modules for the first time, Deno will not redownload
them and will keep using the local cache until it is explicitly asked not to.

To check local cache configuration execute following command:

```shell
deno info
```

```shell
DENO_DIR location: "/Users/<username>/Library/Caches/deno"
Remote modules cache: "/Users/<username>/Library/Caches/deno/deps"
Emitted modules cache: "/Users/<username>/Library/Caches/deno/gen"
Language server registries cache: "/Users/<username>/Library/Caches/deno/registries"
```

To run this sample application need to allow network access:

```shell
deno run --allow-net hello-http-server.js
```

Now the running server can be tested with `curl`:

```shell
curl localhost:8080
```

Which will respond:

```shell
Hello deno
```

### Managing dependencies

The proposed solution to keep track of dependencies, which is no more than a
suggestion, is to use a file that exports all the required dependencies and
place them in a single file that contains URLs.

Create a file called `deps.js` and add our dependencies there.

#### Integrity checking

To create our first lock file, let's run the following command:

```shell
deno cache --lock=lock.json --lock-write deps.js
```

It is also possible to use the `--lock` flag with the run command to enable
runtime verification:

```shell
deno run --lock=lock.json --allow-net hello-http-server.js
```

### Import maps

Deno supports _import maps_.

To run it, we will use the `--import-map` flag. By doing this, we can select the
file where the import maps are.

```shell
deno run --allow-net --import-map=import-map.json hello-http-server.js
```

## Exploring the documentation

To have a look at the documentation for the standard library's HTTP module
execute following command:

```shell
deno doc https://deno.land/std@0.93.0/http/server.ts
```

Deno has a built-in API. To check out its documentation, the following command
should be run:

```shell
deno doc --builtin
```

## Running and installing scripts

Deno allows to run remote scripts:

```shell
deno run https://deno.land/std@0.93.0/examples/welcome.ts
```

### Installing utility scripts

Deno provides the `install` command.

This command wraps a program into a thin shell script and puts it into the
installation `bin` directory. The permissions for the script are set at the time
of its installation and never asked for again:

```shell
deno install --allow-net --allow-read https://deno.land/std@0.93.0/http/file_server.ts
```

This is another module from the standard library called `file_server`. It
creates an HTTP server that serves the current directory.

To give it a name other than `file_server`, can use the `-n` flag, like so:

```shell
deno install --allow-net --allow-read -n serve https://deno.land/std@0.93.0/http/file_server.ts
```

Now, let's serve the current directory:

```shell
serve
```

```shell
HTTP server listening on http://0.0.0.0:4507/
```

This works with remote URLs but can also work with local ones.

## Permissions

**-A, --allow-all** This disables all permission checks.

**--allow-env** This grants access to the environment.

**--allow-hrtime** This grants access to high-resolution time management.

**--allow-net=\<domains\>** This grants access to the network. When used without
arguments, it allows all network access. When used with arguments, it allows us
to pass a comma-separated list of domains where network communication will be
allowed.

**--allow-plugin** This allows plugins to be loaded.

**--allow-read=\<paths\>** This grants read access to the filesystem. When used
without arguments, it grants access to everything the user has access to. With
arguments, this only allows access to the folders provided by a comma-separated
list.

**--allow-run** This grants access to running subprocesses (for instance, with
`Deno.run`).

**--allow-write=\<paths\>** This grants filesystem write access. When used
without arguments, it grants access to everything the user has access to. With
arguments, this only allows access to the folders provided by a comma-separated
list.

## Using the test command

Deno `test` command will run any files with the `js`, `mjs`, `ts`, `jsx`, and
`tsx` extensions and that have `test` in their name preceded by an underscore
(`_`) or a dot (`.`):

```shell
deno test
```

which will output something like this:

```shell
running 2 tests
test first test ... ok (5ms)
test second test ... ok (2ms)

test result: ok. 2 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out (9ms)
```

### Filtering tests

It is possible to use the `--filter` flag and pass a string or pattern that will
match the test names:

```shell
deno test --filter second
```

```shell
running 1 tests
test second test ... ok (2ms)

test result: ok. 1 passed; 0 failed; 0 ignored; 0 measured; 1 filtered out (3ms)
```

### Fail fast

To make build process fail fast as soon as any test fails, the `--fail-fast`
flag can be used.

## Formatting and linting

### Format

To format Deno's code, the CLI provides the fmt command:

```shell
deno fmt
```

To format a single file, can send it as an argument.

To make the formatter ignore a single line or a complete file, can use the `ignore` comment:
```javascript
// deno-fmt-ignore
const book = 'Deno 1.x – Web Development'; 
```
This ignores the line right after the comment.

To ignore entire file use this comment:
```javascript
// deno-fmt-ignore-file
const book = 'Deno 1.x – Web Development';
```

### Lint

```shell
deno lint --unstable to-lint.js
```
```shell
(no-debugger) `debugger` statement is not allowed
    debugger;
    ^^^^^^^^^
    at /<projects directory>/deno-web-development/ch02/hello-world/to-lint.js:4:4

    hint: Remove the `debugger` statement

Found 1 problem
Checked 1 file
```

## Bundling code

To bundle get-current-time.js script use following command:
```shell
deno bundle get-current-time.js bundle.js
```
```shell
Bundle file:///<projects directory>/deno/deno-web-development/ch02/hello-world/get-current-time.js
Emit "bundle.js" (100B)
```

To run newly create bundle script execute following command:
```shell
deno run bundle.js
```

It can also be executed it with Node.js:
```shell
node bundle.js
```

To use this same code in the browser, use a file named `index-bundle.html`, which imports generated bundle.
