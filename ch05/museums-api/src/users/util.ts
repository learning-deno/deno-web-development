import {createHash, encodeToString} from "../deps.ts";

export const hashWithSalt = (text: string, salt: string) => {
    return createHash("sha512")
        .update(`${text}${salt}`)
        .toString();
};

export const generateSalt = () => {
    const arr = new Uint8Array(64);
    crypto.getRandomValues(arr);

    return encodeToString(arr);
};